package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
   @Autowired
   RestTemplate restTemplate;
//its a class which can be used to call any external web service
   @RequestMapping(value = "/Consumer")
   public String getProductList() {
      HttpHeaders headers = new HttpHeaders();
	  headers.setBearerAuth("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqYXZhIiwiZXhwIjoxNjY4NDIwOTU0LCJpYXQiOjE2Njg0MDI5NTR9.xxAF3xw-e8FYMhbNSbCQBqeFA1WzddL0h3qpA44f1DUnHEZJbzUyvNmhLzXreLWMLmUsKNU9cvCaZpPN_zaccQ");
     // headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
      HttpEntity <String> entity = new HttpEntity<String>(headers);
      
      return restTemplate.exchange("http://localhost:8090/admin2", HttpMethod.GET, entity, String.class).getBody();
   }
}
