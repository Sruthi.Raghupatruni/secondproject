//package declaration
package com.book.sruthi.controller;
//importing necessary packages
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.book.sruthi.repositories.*;
import com.book.sruthi.model.*;

//@Controller indicates a particular class serves the role of a controller
@Controller
public class UserController {
	//@Autowired used to autowire bean on the setter method
	@Autowired
	UserRepository urep;
	@Autowired
	BookRepository brep;
	
	//@RequestMapping used to map web requests onto specific handler classes or methods.
	@RequestMapping("/User")
	public ModelAndView getRegForm() {
		return new ModelAndView("UserIndex");	//it allows to return both model and view in one return value
		
	}
	
	//user registration
	@RequestMapping("/reg")
	public ModelAndView getReg(){
        return new ModelAndView("UserReg","userdata",new User());
    }
	@RequestMapping("/addUser")
	public ModelAndView user(@ModelAttribute("userdata") User u) {
		urep.save(u);
		return new ModelAndView("RegSuccess");
	}
	
	//user login
	@RequestMapping("/userlogin")
    public ModelAndView getLoginForm()
    {
        return new ModelAndView("UserLogin","userlogin",new User());
    }
	@RequestMapping("/userlog")
    public ModelAndView checkUser(@ModelAttribute ("userlogin") User u2)
    {
        int uid=u2.getUserId();
        String pwd=u2.getPassword();
        List<User> ulist=urep.findAll();
        for(User u1:ulist)
        {
            if(u1.getUserId()==uid && u1.getPassword().equals(pwd))
                return new ModelAndView("UserMenu");
        }
        return new ModelAndView("ReturnUserLogin");
    }
	
	//request mapping for user menu
	@RequestMapping("/usermenu")
	public String getIndex() {
		return "UserMenu";
	}
	
	//it lists all the books available int the database
	@RequestMapping("/listBook1")
	public ModelAndView listBook(ModelAndView model) throws IOException
    {
    	List<Book> listBook=brep.findAll();
    	model.addObject("listBook1",listBook);
    	model.setViewName("BookUser");
    	return model;
    }
	
	//user can add category to the books
	@RequestMapping("/category")
	public ModelAndView addcategory(HttpServletRequest request) {
		int bookid=Integer.parseInt(request.getParameter("id"));
		Optional<Book> blist=brep.findById(bookid);
		Book bfind=null;
		if(blist.isPresent()) {
			bfind=blist.get();	
		}
		return new ModelAndView("Category","addcategory",bfind);
	}
	@Transactional
	@RequestMapping("/addcat")
	public ModelAndView getcat(@ModelAttribute("addcategory") Book b) {
		brep.updatecategory(b.getCategory(),b.getBookId());
		return new ModelAndView("redirect:/listBook1");
	}
	
	//user can search books
	@RequestMapping("/searchbooks")
	public ModelAndView searchb() {
		return new ModelAndView("SearchBooks");
		
	}
	
	//search by author name
	@RequestMapping("/authorsearch")
	public ModelAndView searchauthor()
    {
    	return new ModelAndView("SearchAuthor","sauthor",new Book());
    }
	@RequestMapping("/searchauthor")
	public ModelAndView sauthor(@ModelAttribute("sauthor") Book b)
	{
		List<Book> bookauthor=brep.findByAuthor(b.getAuthor());
		return new ModelAndView("AuthorDisplay","displayauthor",bookauthor);
	}
	
	//search by book name
	@RequestMapping("/titlesearch")
	public ModelAndView searchtitle()
    {
    	return new ModelAndView("SearchTitle","stitle",new Book());
    }
	@RequestMapping("/searchtitle")
	public ModelAndView stitle(@ModelAttribute("stitle") Book b)
	{
		List<Book> booktitle=brep.findByBookName(b.getBookName());
		return new ModelAndView("TitleDisplay","displaytitle",booktitle);
	}
	
	//search by publication
	@RequestMapping("/publicationsearch")
	public ModelAndView searchpublication()
    {
    	return new ModelAndView("SearchPublication","spublication",new Book());
    }
	@RequestMapping("/searchpublication")
	public ModelAndView spub(@ModelAttribute("spublication") Book b)
	{
		List<Book> bookpub=brep.findByPublication(b.getPublication());
		//Optional<Book> bookpub1=brep.findById(b.getBookId());
		return new ModelAndView("PublicationDisplay","displaypublication",bookpub);
	}
	
	//search by id
	@RequestMapping("/idsearch")
	public ModelAndView searchid()
    {
    	return new ModelAndView("SearchId","sid",new Book());
    }
	@RequestMapping("/searchid")
	public ModelAndView sid(@ModelAttribute("sid") Book b)
	{
		
		List<Book> bookid=brep.findByBookId(b.getBookId());
		return new ModelAndView("IdDisplay","displayid",bookid);
	}
	
	//sorting by price in ascending order
	@RequestMapping("/sortprice")
	public ModelAndView sort(ModelAndView model) throws IOException
    {
    	List<Book> sortasc=brep.findByOrderByPriceAsc();
    	model.addObject("sortprice",sortasc);
    	model.setViewName("SortPrice");
    	return model;
    }
	
	//sorting by particular price range
	@RequestMapping("/range")
	public ModelAndView prange(ModelAndView model) throws IOException{
		List<Book> sortrange=brep.getListByPrice(500,2000);
    	model.addObject("range",sortrange);
    	model.setViewName("PriceRange");
    	return model;
	}
	

}
