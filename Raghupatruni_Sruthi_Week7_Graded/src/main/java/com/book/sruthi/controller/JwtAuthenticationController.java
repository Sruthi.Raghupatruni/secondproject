package com.book.sruthi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.book.sruthi.config.JwtTokenUtil;
import com.book.sruthi.model.*;
import com.book.sruthi.service.JwtUserDetailsService;
//to remove the redundancy of declaring the @ResponseBody annotation in your controller
@RestController
@CrossOrigin
public class JwtAuthenticationController {
	//@Autowired used to autowire bean on the setter method
   @Autowired
    private AuthenticationManager authenticationManager;
   @Autowired
    private JwtTokenUtil jwtTokenUtil;

   @Autowired
    private JwtUserDetailsService userDetailsService;


//@RequestMapping used to map web requests onto specific handler classes or methods.
   @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@ModelAttribute("authenticate") JwtRequest authenticationRequest) throws Exception {



       authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());



       final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());



       final String token = jwtTokenUtil.generateToken(userDetails);



       return ResponseEntity.ok(new JwtResponse(token));
    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@ModelAttribute("userobj") UserDTO user) throws Exception {
        return ResponseEntity.ok(userDetailsService.save(user));
    }



   private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}

