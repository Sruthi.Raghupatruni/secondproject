package com.book.sruthi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.book.sruthi.model.*;
//used to remove the redundancy of declaring the @ResponseBody annotation in your controller
@RestController
@CrossOrigin()
public class HelloWorldController {

	@RequestMapping({ "/hello" })
	public String hello() {
		return "hello";
	}
	//registration for user to generate jwt token
	@RequestMapping("/UserDet")
	public ModelAndView insertForm() {
		return new ModelAndView("UserJwt","userjwt",new UserDTO());
	}
	//login for user to generate jwt token
	@RequestMapping("/login")
	public ModelAndView loginForm() {
		return new ModelAndView("LoginJwt","loginjwt",new UserDTO());
	}
}
