//package declaration
package com.book.sruthi.controller;
//importing necessary packages
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.book.sruthi.model.Admin;
import com.book.sruthi.model.Book;
import com.book.sruthi.repositories.*;

//@Controller indicates a particular class serves the role of a controller
@Controller
@RequestMapping("/admin2")
public class AdminController {
	//@Autowired used to autowire bean on the setter method
	@Autowired
	AdminRepository arep;
	@Autowired
	BookRepository brep;
	
	//@RequestMapping used to map web requests onto specific handler classes or methods.
	@RequestMapping("/")
	public String welcome() {
		return "Welcome";
	}
	
	//Requesting mapping for index page
	@RequestMapping("/index")
	public String getIndex() {
		return "Index";
	}
	
	@RequestMapping("/AdminMenu")
	public ModelAndView getRegForm1() {
		return new ModelAndView("AdminMenu");	//it allows to return both model and view in one return value
		
	}
	@RequestMapping("/adreg")
	public ModelAndView getReg(){
        return new ModelAndView("AdminReg","admindata",new Admin());
    }
	@RequestMapping("/addAdmin")
	public ModelAndView user(@ModelAttribute("admindata") Admin a) {
		arep.save(a);
		return new ModelAndView("RegSuccessA");
	}
	
	//Request mapping admin login
	@RequestMapping("/Admin")
    public ModelAndView getLoginForm()
    {
        return new ModelAndView("AdminLogin","adminlog",new Admin());	//it allows to return both model and view in one return value
    }
    @RequestMapping("/adminlogin")
    public ModelAndView checkUser(@ModelAttribute ("adminlog") Admin a)
    {
    	String ad=a.getAdminName();
        String pwd=a.getAdminPassword();
        List<Admin> alist=arep.findAll();
        for(Admin a1:alist)
        {
            if(a1.getAdminName().equals(ad) &&a1.getAdminPassword().equals(pwd))
                return new ModelAndView("AdminIndex");
        }
        return new ModelAndView("ReturnAdmin");
    }
    
    //Request mapping for admin menu
    @RequestMapping("/adminmenu")
	public String getIndex1() {
		return "AdminIndex";
	}
    
    //it lists all the books available int the database
    @RequestMapping("/listBook")
	public ModelAndView listItem(ModelAndView model) throws IOException
    {
    	List<Book> listBook=brep.findAll();
    	model.addObject("listBook",listBook);
    	model.setViewName("Book");
    	return model;
    }
    
    //admin can add new book to the book store
    @RequestMapping("/addnewbook")
	public ModelAndView item() {
		return new ModelAndView("AddNewBook","bookdata1",new Book());
	}
	@RequestMapping("/addnBook")
	public ModelAndView contact1(@ModelAttribute("bookdata1") Book b) {
		brep.save(b);
		return new ModelAndView("success");
	}
	
	//admin can update the items in the list
	@RequestMapping("/updateBook")
    public ModelAndView editB(HttpServletRequest request) {
        int bookId = Integer.parseInt(request.getParameter("id"));
        Book b = brep.getById(bookId);
        ModelAndView model = new ModelAndView("UpdateBook");
        model.addObject("updatebook", b);
        return model;
    }
	@Transactional
    @RequestMapping("/bpu")
    public ModelAndView getbpd(@ModelAttribute("updatebook")Book b) {
        brep.update(b.getBookName(),b.getAuthor(),b.getPublication(),b.getPrice(),b.getBookId());
        return new ModelAndView("success");
    }
	
	//admin can delete the books present in the list
	@RequestMapping("/deleteBook")
    public ModelAndView deleteItem(HttpServletRequest request) {
    int bookid= Integer.parseInt(request.getParameter("id"));
    brep.deleteById(bookid);
    return new ModelAndView("success");
    }
	
	//admin can search the books 
	@RequestMapping("/searchbooks1")
	public ModelAndView searchb2() {
		return new ModelAndView("SearchBooksA");
		
	}
	
	//searching by using author name
	@RequestMapping("/authorsearch1")
	public ModelAndView searchauthor()
    {
    	return new ModelAndView("SearchAuthorA","sauthor1",new Book());
    }
	@RequestMapping("/searchauthor1")
	public ModelAndView sauthor1(@ModelAttribute("sauthor1") Book b)
	{
		List<Book> bookauthor1=brep.findByAuthor(b.getAuthor());
		return new ModelAndView("AuthorDisplayA","displayauthor1",bookauthor1);
	}
	
	//searching by using book id
	@RequestMapping("/idsearch1")
	public ModelAndView searchid()
    {
    	return new ModelAndView("SearchIdA","sid1",new Book());
    }
	@RequestMapping("/searchid1")
	public ModelAndView sid1(@ModelAttribute("sid1") Book b)
	{
		
		List<Book> bookid=brep.findByBookId(b.getBookId());
		return new ModelAndView("IdDisplayA","displayid1",bookid);
	}

}
