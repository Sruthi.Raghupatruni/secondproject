//package declaration
package com.book.sruthi.model;

//importing necessary packages
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

//@Component allows Spring to automatically detect our custom beans
@Component

//@Entity annotation specifies that the class is an entity and is mapped to a database table
@Entity

//table creation in database
@Table(name="admin")

//admin class
public class Admin {
	//primary key
	@Id
	//columns present in table
	@Column(name="adminName")
	private String adminName;
	@Column(name="adminPassword")
	private String adminPassword;
	
	//getters and setters
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	
}