//package declaration
package com.book.sruthi.model;

//importing necessary packages
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

//@Component allows Spring to automatically detect our custom beans
@Component

//@Entity annotation specifies that the class is an entity and is mapped to a database table
@Entity

//table creation in database
@Table(name="user")

//user class
public class User {
	
	//primary key
	@Id
	//columns present in table
	@Column(name="userId")
	private int userId;
	@Column(name="userName")
	private String userName;
	@Column(name="password")
	private String password;
	
	//getters and setters
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
