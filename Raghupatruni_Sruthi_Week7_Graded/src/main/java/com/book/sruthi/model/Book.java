//package declaration
package com.book.sruthi.model;
//importing necessary packages
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

//@Component allows Spring to automatically detect our custom beans
@Component

//@Entity annotation specifies that the class is an entity and is mapped to a database table
@Entity

//table creation in database
@Table(name="book")

public class Book {
	//primary key
	@Id
	//table creation in database
	@Column(name="bookId")
	private int bookId;
	@Column(name="bookName")
	private String bookName;
	@Column(name="author")
	private String author;
	@Column(name="publication")
	private String publication;
	@Column(name="price")
	private double price;
	@Column(name="category")
	private String category;
	
	//getters and setters
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublication() {
		return publication;
	}
	public void setPublication(String publication) {
		this.publication = publication;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	

}
