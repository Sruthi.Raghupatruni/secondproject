package com.book.sruthi.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.book.sruthi.model.Book;
@Repository
public interface BookRepository extends JpaRepository<Book,Integer>{
@Modifying
	
	@Query("update Book b set b.bookName=?1,b.author=?2,b.publication=?3,b.price=?4 where b.bookId=?5")

	public void update(String bookName,String author,String publication,double price, int bookId);
@Modifying
	@Query("update Book b set b.category=?1 where b.bookId=?2")
	public void updatecategory(String category, int bookId);

public List<Book> findByAuthor(String author);
public List<Book> findByBookName(String bookName);
public List<Book> findByPublication(String publication);
public List<Book> findByBookId(int bookId);
public List<Book> findByOrderByPriceAsc();
@Query("select b from Book b where b.price>?1 and b.price<?2")
public List<Book> getListByPrice(double price1,double price2);
}
