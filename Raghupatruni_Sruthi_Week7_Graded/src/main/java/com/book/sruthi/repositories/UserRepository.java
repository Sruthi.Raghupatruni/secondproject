package com.book.sruthi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.book.sruthi.model.User;
public interface UserRepository extends JpaRepository<User,Integer>{

}
