
package com.book.sruthi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.book.sruthi.model.DAOUser;

@Repository
public interface UserDao extends JpaRepository<DAOUser,Integer>
{		
	DAOUser findByUsername(String username);
}

