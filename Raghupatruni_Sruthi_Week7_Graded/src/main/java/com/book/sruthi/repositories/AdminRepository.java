package com.book.sruthi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.book.sruthi.model.Admin;

public interface AdminRepository extends JpaRepository<Admin,Integer>{

}
