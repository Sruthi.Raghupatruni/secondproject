package com.book.sruthi.service;
import java.util.*;
public class AdminBusiness {
	public AdminService adminservice;
	public AdminBusiness(AdminService adminservice) {
	this.adminservice=adminservice;
	}
	public List<String> getadmindetails(String adminName,String adminPassword){
		List<String> retreiveadmindetails=new ArrayList<String>();
		List<String> adminname=adminservice.getadminname(adminName);
		List<String> password=adminservice.getpassword(adminPassword);
		for(String adname:adminname) {
		for(String p:password) {
			if(adname.contains("admin")&&p.contains("admin")) {
				retreiveadmindetails.add(adname);
				retreiveadmindetails.add(p);
				}
			}
		}
		return retreiveadmindetails;
	}
	
}
