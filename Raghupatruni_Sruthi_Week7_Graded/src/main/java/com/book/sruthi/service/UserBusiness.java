package com.book.sruthi.service;

import java.util.*;
public class UserBusiness {
	public UserService userservice;
	public UserBusiness(UserService userservice) {
	this.userservice=userservice;
	}
	public List<String> getuserdetails(String userName,String password){
		List<String> retreiveuserdetails=new ArrayList<String>();
		List<String> username=userservice.getusername(userName);
		List<String> upassword=userservice.getuserpassword(password);
		for(String usname:username) {
		for(String userp:upassword) {
			if((usname.contains("user"))&&(userp.contains("user"))) {
				retreiveuserdetails.add(usname);
				retreiveuserdetails.add(userp);
				}
			}
		}
		return retreiveuserdetails;
	}
	
}
