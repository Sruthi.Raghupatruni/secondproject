package com.book.sruthi.service;

import java.util.List;

public interface UserService {
	public List<String> getusername(String username);
	public List<String> getuserpassword(String userpassword);

}
