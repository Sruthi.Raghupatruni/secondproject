<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
<h1 style="color:#48D1CC">SEARCH</h1>
<br>
<h3><a href="publicationsearch">Search By Publication</a></h3>
<h3><a href="titlesearch">Search By Title</a></h3>
<h3><a href="authorsearch">Search By Author</a></h3>
<h3><a href="idsearch">Search By Book Id</a></h3>
<h3><a href="usermenu">Return to User Menu</a></h3>
<h3><a href="index">Logout from User</a></h3>
</div>
</body>
</html>
