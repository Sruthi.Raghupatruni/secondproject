<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Information</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
<br><br>
	<form:form action="addcat" modelAttribute="addcategory">
	<h1 style="color: #48D1CC">ADD CATEGORY</h1>
		<table>
			<tr>
				<td><form:input type="hidden" path="bookId" /></td>
			</tr>
			<tr>
				
				<td><form:input type="hidden" path="bookName" /></td>
			</tr>
			<tr>
				
				<td><form:input type="hidden" path="author" /></td>
			</tr>
			<tr>
				
				<td><form:input type="hidden" path="publication" /></td>
			</tr>
			<tr>
				
				<td><form:input type="hidden" path="price" /></td>
			</tr>
			<tr>
				<td>Category</td>
				<td><form:input type="type" path="category" /></td>
			</tr>
		
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Add Category"></td>
			</tr>
		</table>
	</form:form>
	</div>
</body>
</html>