<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book information</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
	<form:form action="searchauthor" modelAttribute="sauthor">
	<br><br><br>
		<table>
			
			<tr>
				<td>Author</td>
				<td><form:input type="text" path="author" /></td>
			</tr>
			
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Search"></td>
			</tr>
		</table>
		
	</form:form>
	</div>
</body>
</html>