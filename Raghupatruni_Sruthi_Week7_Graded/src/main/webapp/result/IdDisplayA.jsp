<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MAGIC OF BOOKS</title>
        <style>
body {
  background-color: #FFFFCC;
}
</style>
    </head>
    <body>
    <form:form  modelAttribute="displayid1"> 
        <div align="center">
        <br><br>
            <h1 style="color:#48D1CC">BOOKS</h1>
           
            <table border="1">
            <tr>
                <th>Book Id</th>
                <th>Book Name</th>
                <th>Author</th>
                <th>Publication</th>
                <th>Price</th>
                
                </tr>
                <c:forEach var="book" items="${displayid1}" varStatus="status">
                <tr>
                	<td>${book.bookId}</td>
                    <td>${book.bookName}</td>
                    <td>${book.author}</td>
                    <td>${book.publication}</td>
                    <td>${book.price}</td>
                    
                             
                </tr>
                </c:forEach>             
            </table>
            <br><br><br>
            <h3><a href="adminmenu">Return to Admin Menu</a></h3>
            <h3><a href="index">Logout from Admin</a></h3>
        </div>
        </form:form>
    </body>
</html>