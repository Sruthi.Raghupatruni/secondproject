<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index Page</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>

</head>
<body>
<div align="center">
<br><br>
<h1 style="color:#48D1CC">WELCOME TO BOOK STORE</h1>
<h2><a href="AdminMenu">As Admin</a></h2>
<h2><a href="User">As User</a></h2>
<h2><a href="UserDet">Jwt Checking</a></h2>
<br><br><br><br><br>
*NOTE*
<br><br><br>
[For JWT after registering user should give url mapping as "/login" for logging into the application]
<br><br>
[For JWT after token generation user should give url mapping as "/index" for entering into the Book Application]
</div>
</body>
</html>