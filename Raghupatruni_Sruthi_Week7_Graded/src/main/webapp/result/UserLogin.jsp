<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Information</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
<br><br>
	<form:form action="userlog" modelAttribute="userlogin">
		<h1 style="color:#48D1CC"align="center">USER LOGIN</h1>	
		<table>
			<tr>
				<td>User Id</td>
				<td><form:input type="text" path="userId" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><form:input type="text" path="password" /></td>
			</tr>
			
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Login"></td>
			</tr>
		</table>
	</form:form>
	</div>
</body>
</html>