<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri = "http://www.springframework.org/tags/form"
prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Login</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
<br><br>
<h1 style="color:#48D1CC">ADMIN LOGIN</h1>
<form:form method = "POST" action ="adminlogin" modelAttribute="adminlog">
<table>
            <tr>
               <td><form:label path = "adminName">Admin Name</form:label></td>
               <td><form:input path = "adminName" /></td>
            </tr>
            <tr>
               <td><form:label path = "adminPassword">Admin Password</form:label></td>
               <td><form:input path = "adminPassword" /></td>
            </tr>
            <tr>
               <td align="center" colspan="2"><input type="submit"
					value="login"></td>
            </tr>
         </table>  
      </form:form>
</div>
</body>
</html>