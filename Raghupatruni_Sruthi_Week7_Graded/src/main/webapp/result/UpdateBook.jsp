<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book information</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
<br><br>
		<h1 style="color:#48D1CC">UPDATE BOOK</h1>
				
	<form:form action="bpu" modelAttribute="updatebook">
		<table>
			
			<tr>
				<td>Book Id</td>
				<td><form:input type="text" path="bookId" /></td>
			</tr>
			<tr>
				<td>Book Name</td>
				<td><form:input type="text" path="bookName" /></td>
			</tr>
			<tr>
				<td>Author</td>
				<td><form:input type="text" path="author" /></td>
			</tr>
			<tr>
				<td>Publication</td>
				<td><form:input type="text" path="publication" /></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><form:input type="text" path="price" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Update"></td>
			</tr>
		</table>
	</form:form>
	</div>
</body>
</html>