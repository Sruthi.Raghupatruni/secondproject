<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Information</title>
<style>
body {
  background-color: #FFFFCC;
}
</style>
</head>
<body>
<div align="center">
<br><br>
	<form:form action="addnBook" modelAttribute="bookdata1">
	<h1 style="color:#48D1CC">ADD NEW BOOK</h1>
		<table>
			
			<tr>
				<td>Book Id</td>
				<td><form:input type="text" path="bookId" /></td>
			</tr>
			<tr>
				<td>Book Name</td>
				<td><form:input type="text" path="bookName" /></td>
			</tr>
			<tr>
				<td>Author</td>
				<td><form:input type="text" path="author" /></td>
			</tr>
			<tr>
				<td>Publication</td>
				<td><form:input type="text" path="publication" /></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><form:input type="text" path="price" /></td>
			</tr>
			
     
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Add"></td>
			</tr>
		</table>
	</form:form>
	</div>
</body>
</html>