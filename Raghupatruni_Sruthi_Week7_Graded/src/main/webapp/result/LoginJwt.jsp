<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri = "http://www.springframework.org/tags/form"
prefix = "form"%>
<html>
   <head>
      <title>User Form</title>
      <style>
body {
  background-color: #FFFFCC;
}
</style>
   </head>
  <body>
  <div align="center">
<br><br>
      <h1 style="color:#48D1CC">USER lOGIN FOR JWT TOKEN</h1>
      <form:form method = "POST" action = "authenticate" modelAttribute="loginjwt">
         <table>
            <tr>
               <td><form:label path = "username">User Name</form:label></td>
               <td><form:input path = "username" /></td>
            </tr>
            <tr>
               <td><form:label path = "password">Password</form:label></td>
               <td><form:input path = "password" /></td>
            </tr>
            <tr>
             <td align="center" colspan="2"><input type="submit"
					value="login"></td>
            </tr>
         </table>  
      </form:form>
     <br><br><br><br><br>

[After token generation user should give url mapping as "/index" for entering into the Book Application]
      </div>
   </body>
   
</html>