package com.book.sruthi;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.book.sruthi.service.AdminBusiness;
import com.book.sruthi.service.AdminService;

@SpringBootTest
public class AdminMock {
	@Test
	public void testadmindetails() {
		AdminService adservice=mock(AdminService.class);
		List<String> anamelist=Arrays.asList("admin");
		when(adservice.getadminname("dummy")).thenReturn(anamelist);
		AdminBusiness adbusiness=new AdminBusiness(adservice);
		List<String> admin1=adbusiness.getadmindetails("dummy", "dummy");
		System.out.println(admin1);
	}

}
