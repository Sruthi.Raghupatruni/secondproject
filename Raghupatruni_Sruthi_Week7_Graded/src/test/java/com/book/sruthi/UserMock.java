package com.book.sruthi;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.book.sruthi.service.UserBusiness;
import com.book.sruthi.service.UserService;

@SpringBootTest
public class UserMock {
	@Test
	public void testuserdetails() {
		UserService usservice=mock(UserService.class);
		List<String> unamelist=Arrays.asList("user");
		when(usservice.getusername("dummy")).thenReturn(unamelist);
		UserBusiness usbusiness=new UserBusiness(usservice);
		List<String> user1=usbusiness.getuserdetails("dummy", "dummy");
		System.out.println(user1);
	}

}
